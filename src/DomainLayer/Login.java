package DomainLayer;

import MapperLayer.*;
import javax.swing.*;

public class Login 
{
	public Login()
	{
	     
	}
	
	public boolean verifyLogin(int type, User u)//admin=1 employee=0
	{
		if(type==1)
		{
			AdminMapper a=new AdminMapper();
			return a.verifyAdmin(u);
		}
		else 
			if(type==0)
			{
				EmployeeMapper e=new EmployeeMapper();
				return e.verifyEmployee(u);
			}
			else
			{
				JOptionPane.showMessageDialog(null,"Select a user type :)");
				return false;
			}
	}
}
