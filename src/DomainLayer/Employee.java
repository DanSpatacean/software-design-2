package DomainLayer;

import Presentation.*;
import MapperLayer.*;
import java.util.ArrayList;
import java.util.Vector;

public class Employee extends User 
{
	private int id;
    private String user;
    private String pass;
    private String name;
    
    private ArrayList<Customer> cList;
    private ArrayList<Account> aList;
    private ArrayList<Transaction> accTrList;
    
    public Employee(String user, String pass) 
    {
        super(user, pass);
        id=getIdFromDB();
    }
    
    public void setId(int id) 
    {

        this.id = id;
    }

    public void setUser(String user) 
    {
        this.user = user;
    }

    public void setPass(String pass) 
    {
        this.pass = pass;
    }

    public void setName(String name) 
    {
        this.name = name;
    }
    
    public int getId() 
    {
        return id;
    }

    public String getName() 
    {
        return name;
    }

    public String getUser() 
    {
        return user;
    }

    public String getPass() 
    {
        return pass;
    }
    
    public ArrayList<Customer> getCust() 
    {
        return cList;
    }

    public ArrayList<Account> getAcc() 
    {
        return aList;
    }
    
    public ArrayList<Transaction> getAccTr() 
    {
        return accTrList;
    }
    
    public void setCust() 
    {
        EmployeeMapper e = new EmployeeMapper();
        cList = e.getCustomerList();

    }

    public void setAcc() 
    {
        EmployeeMapper e = new EmployeeMapper();
        aList = e.getAccountList();

    }
    
    public void setAccTr() 
    {
        EmployeeMapper e = new EmployeeMapper();
        accTrList = e.getAccountTrList();

    }
    
    public int getIdFromDB()
    {
        EmployeeMapper e=new EmployeeMapper();
        int i=e.getEmployeeId("user");
        return i;
    }
    
    public String getSelectedName(int id) 
    {
        for (int i = 0; i < this.cList.size(); i++) 
        {
            if (this.cList.get(i).getId() == id) 
            {
                return cList.get(i).getName();
            }
        }
        return "null";
    }
    
    public String getSelectedMail(int id) 
    {
        for (int i = 0; i < this.cList.size(); i++) 
        {
            if (this.cList.get(i).getId() == id) 
            {
                return cList.get(i).getMail();
            }
        }
        return "null";
    }
    
    public String getSelectedCity(int id) 
    {
        for (int i = 0; i < this.cList.size(); i++) 
        {
            if (this.cList.get(i).getId() == id) 
            {
                return cList.get(i).getCity();
            }
        }
        return "null";
    }
    
    public int getSelectedType(int id) 
    {
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() == id) 
            {
                return aList.get(i).getType();
            }
        }
        return 0;
    }
    
    public int getSelectedCid(int id) 
    {
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() == id) 
            {
                return aList.get(i).getCustId();
            }
        }
        return 0;
    }
    
    public int getSelectedBalance(int id) 
    {
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() == id) 
            {
                return aList.get(i).getBalance();
            }
        }
        return 0;
    }
    
    public String getSelectedCurrency(int id) 
    {
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() == id) 
            {
                return aList.get(i).getCurrency();
            }
        }
        return "null";
    }
    
    public String getSelectedDate(int id) 
    {
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() == id) 
            {
                return aList.get(i).getCrDate();
            }
        }
        return "null";
    }
    
    public void addCustomer(Customer e) 
    {
        cList.add(e);

        EmployeeMapper a = new EmployeeMapper();
        a.createCustomer(e.getId(), e.getName(), e.getMail(), e.getCity());
    }
    
    public void updateCustById(int id, String name, String mail, String city) 
    {   //update in arrayList
        ArrayList<Customer> newList = new ArrayList<Customer>();
        for (int i = 0; i < this.cList.size(); i++) 
        {
            if (this.cList.get(i).getId() != id) 
            {
                newList.add(cList.get(i));
            } 
            else 
            {
                cList.get(i).setName(name);
                cList.get(i).setMail(mail);
                cList.get(i).setCity(city);
            }
        }
        cList = newList;

        //update in db
        EmployeeMapper a = new EmployeeMapper();
        a.updateCustomer(id, name, mail, city);
    }
    
    public void updateAccById(int acid, String type, String cid, String balance, String currency, String date) 
    {
    	//update in arrayList
        ArrayList<Account> newList = new ArrayList<Account>();
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() != acid) 
            {
                newList.add(aList.get(i));
            } 
            else 
            {
                aList.get(i).setId(acid);
                aList.get(i).setType(type);
                aList.get(i).setCustid(cid);
                aList.get(i).setBalance(balance);
                aList.get(i).setCurrency(currency);
                aList.get(i).setCrDate(date);

            }
        }
        aList = newList;

        //update in db
        EmployeeMapper a = new EmployeeMapper();
        a.updateAccount(acid, type, cid, balance, currency, date);
    }
    
    public void deteleAccById(int aid) 
    {   //delete from arrayList
        ArrayList<Account> newList = new ArrayList<Account>();
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() != aid) 
            {
                newList.add(aList.get(i));
            }
        }
        aList = newList;

        //delete from db
        EmployeeMapper a = new EmployeeMapper();
        a.deleteAccount(aid);
    }
    
    public void withdrawAccById(int aid, int amount) 
    {
    	//withdraw form arrayList
        int newBalance = 0;
        boolean ok = false;
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() == aid) 
            {
                aList.get(i).withdraw(amount);
                newBalance = aList.get(i).getBalance();
                ok = true;
            }
        }
        
        //withdraw from db
        if (ok) 
        {
            EmployeeMapper a = new EmployeeMapper();
            a.withdrawAccount(aid, newBalance);
        }
        
    }
    
    public void depositAccById(int aid, int amount) 
    {
        //deposit in arrayList
        int newBalance = 0;
        boolean ok = false;
        for (int i = 0; i < this.aList.size(); i++) 
        {
            if (this.aList.get(i).getId() == aid) 
            {
                aList.get(i).deposit(amount);
                newBalance = aList.get(i).getBalance();
                ok = true;
            }
        }
        
        //deposit in db
        if (ok) {
            EmployeeMapper a = new EmployeeMapper();
            a.depositAccount(aid, newBalance);
        }
    }
    
    public void addAccount(Account ac) 
    {
        aList.add(ac);

       EmployeeMapper a = new EmployeeMapper();
       a.createAccount(ac.getId(), ac.getType(), ac.getCustId(), ac.getBalance(),ac.getCurrency(),ac.getCrDate2());   
    }
    
    public void addTransaction(int eid,int cid, int  aid, String type, int amount) 
    {
        
        Transaction t=new Transaction();
        
        t.setId(accTrList.size());
        t.setEmpId(eid);
        t.setCustId(cid);
        t.setAcc(aid);
        t.setType(type);
        t.setAmount(amount);
        
        accTrList.add(t);
        
        EmployeeMapper a = new EmployeeMapper();
        a.createTr(t.getId(), t.getEmpId(), t.getCustId(), t.getAcc(),t.getType(),t.getAmount(),t.getCrDate2());
        System.out.println("empl id " + t.getEmpId());        
    }
    
    public void genReport(String from, String to, String client)
    {
    	ArrayList<Transaction> newList=new ArrayList<Transaction>();
    	int cid=Integer.parseInt(client);
    	
    	//from
    	int fy = 0, fm = 0, fd = 0;
    	String delim = "-";
    	String[] tokens = from.split(delim);
    	for(int i = 0; i < tokens.length; i++)
    	{
    		fy = fm;
    		fm = fd;
    		fd = Integer.parseInt(tokens[i]);	
    	}
    	
    	//to
    	int ty = 0, tm = 0, td = 0;
        String delim2 = "-";
        String[] tokens2 = to.split(delim2);
        for (int i = 0; i < tokens2.length; i++) 
        {
            ty = tm;
            tm = td;
            td = Integer.parseInt(tokens2[i]);       
        }
        
      //acc tr list
        for (int i = 0; i < this.accTrList.size(); i++) 
        {
            if (this.accTrList.get(i).getCustId() == cid)
                if(fy<=Integer.parseInt(accTrList.get(i).getYear()) && Integer.parseInt(accTrList.get(i).getYear())<=ty)
                {   
                	// System.out.println( fy+ " " +Integer.parseInt(accTrList.get(i).getYear()) +" "+ Integer.parseInt(accTrList.get(i).getYear())+ " " +ty );
                    if(fm<=Integer.parseInt(accTrList.get(i).getMounth()) && Integer.parseInt(accTrList.get(i).getMounth())<=tm)
                    {      
                    	System.out.println( fm+ " " +Integer.parseInt(accTrList.get(i).getMounth()) +" "+ Integer.parseInt(accTrList.get(i).getMounth())+ " " +tm );
                        if(fd<=Integer.parseInt(accTrList.get(i).getDay()) && Integer.parseInt(accTrList.get(i).getDay())<=td)
                            System.out.println("add");
                        newList.add(accTrList.get(i));  
                     } 
                 }         
        }
        
        accTrList=newList;
    }
    
    
}
