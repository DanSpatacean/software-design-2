package DomainLayer;

import MapperLayer.*;
import Presentation.*;
import java.util.ArrayList;
import java.util.*;
import javax.swing.table.DefaultTableModel;

public class Table 
{
	public static DefaultTableModel employeesToTableModel(ArrayList<Employee> list) 
	{
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("idEmployee");
        columnNames.add("user");
        columnNames.add("pass");
        columnNames.add("name");

        for (int i = 0; i < list.size(); i++) 
        {
            Employee empl = list.get(i); 
            Vector<Object> row = new Vector<Object>(); 
            
            row.add(empl.getId());
            row.add(empl.getUser());
            row.add(empl.getPass());
            row.add(empl.getName());
            
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) 
        {
            public boolean isCellEditable(int row, int column) 
            {
                return false;
            }
        };
        return model;
    }
	
	public static DefaultTableModel customersToTableModel(ArrayList<Customer> list) 
	{
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("id");
        columnNames.add("name");
        columnNames.add("mail");
        columnNames.add("city");

        for (int i = 0; i < list.size(); i++) 
        {
            Customer cust = list.get(i); 
            Vector<Object> row = new Vector<Object>();
            
            row.add(cust.getId());
            row.add(cust.getName());
            row.add(cust.getMail());
            row.add(cust.getCity());
            
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) 
        {
            public boolean isCellEditable(int row, int column) 
            {
                return false;
            }
        };
        return model;
    }
	
	public static DefaultTableModel accountsToTableModel(ArrayList<Account> list) 
	{
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("acId");
        columnNames.add("type");
        columnNames.add("cId");
        columnNames.add("balance");
        columnNames.add("currency");
        columnNames.add("created");

        for (int i = 0; i < list.size(); i++) 
        {
            Account ac = list.get(i); 
            Vector<Object> row = new Vector<Object>(); 
     
            row.add(ac.getId());
            row.add(ac.getType());
            row.add(ac.getCustId());
            row.add(ac.getBalance());
            row.add(ac.getCurrency());
            row.add(ac.getCrDate());
      
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) 
        {
            public boolean isCellEditable(int row, int column) 
            {
                return false;
            }
        };
        return model;
    }
	
	public static DefaultTableModel acctrToTableModel(ArrayList<Transaction> list) 
	{
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("trId");
        columnNames.add("eId");
        columnNames.add("cId");
        columnNames.add("type");
        columnNames.add("amount");
        columnNames.add("date");

        for (int i = 0; i < list.size(); i++) 
        {
            Transaction ac = list.get(i); 
            Vector<Object> row = new Vector<Object>(); 

            row.add(ac.getId());
            row.add(ac.getEmpId());
            row.add(ac.getCustId());
            row.add(ac.getType());
            row.add(ac.getAmount());
            row.add(ac.getCrDate());
    
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) 
        {
            public boolean isCellEditable(int row, int column) 
            {
                return false;
            }
        };
        return model;
    }
	
	public static DefaultTableModel etrToTableModel(ArrayList<Transaction> list) 
	{
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("trId");
        columnNames.add("eId");
        columnNames.add("cId");
        columnNames.add("type");
        columnNames.add("amount");
        columnNames.add("date");

        for (int i = 0; i < list.size(); i++) 
        {
            Transaction ac = list.get(i); 
            Vector<Object> row = new Vector<Object>(); 

            row.add(ac.getId());
            row.add(ac.getEmpId());
            row.add(ac.getCustId());
            row.add(ac.getType());
            row.add(ac.getAmount());
            row.add(ac.getCrDate());
  
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) 
        {
            public boolean isCellEditable(int row, int column) 
            {
                return false;
            }
        };
        return model;
    }
}
