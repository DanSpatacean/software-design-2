package DomainLayer;

import java.util.Calendar;
import java.util.Date;

public class Account 
{
	private int id;
	private int type;
	private int cId;
	private int balance;
	private String currency;
	private Calendar creationDate;
	
	public Account(int id, int cId)
	{
		this.id=id;
		this.cId=cId;
		creationDate=Calendar.getInstance();
		creationDate.setTime(new Date());
	}
	
	public void deposit(int value)
	{
			balance+=value;
	}
	
	public void withdraw(int value)
	{
		if(balance>=value)
			balance-=value;
	}
	
	public String getCrDate()
	{
		String date = creationDate.get(Calendar.YEAR) + "-" + (creationDate.get(Calendar.MONTH) + 1) + "-" + creationDate.get(Calendar.DATE);
		return date;
	}
	
	public String getCrDate2() 
	{
        String date = creationDate.get(Calendar.YEAR) + "-" + (creationDate.get(Calendar.MONTH) ) + "-" + creationDate.get(Calendar.DATE);
        return date;
    }
	
	public int getId() 
	{
        return id;
    }

    public int getType() 
    {
        return type;
    }

    public int getCustId() 
    {
        return cId;
    }

    public int getBalance() 
    {
        return balance;
    }

    public String getCurrency() 
    {
        return currency;
    }
    
    public void setId(int val) 
    {
        this.id = val;
    }

    public void setType(String str) 
    {
        type = Integer.parseInt(str);
    }
    
    public void setCustid(String str) 
    {
        type = Integer.parseInt(str);
    }

    public void setBalance(String str) 
    {
        balance = Integer.parseInt(str);
    }

    public void setCurrency(String str) 
    {
        currency = str;
    }

    public void setCrDate(String str) 
    {
        int y=0, m=0, d=0;
        String delim = "-";
        String[] tokens = str.split(delim);
        for (int i = 0; i < tokens.length; i++) 
        {
            y=m;
            m=d;
            d = Integer.parseInt(tokens[i]);  
        }
         creationDate.set(y,m,d);
    }
    
    public static void main(String args[]) 
    {
        Account ac = new Account(1, 1);
        ac.setCrDate("2016-26-03");
        System.out.println(ac.getCrDate());
    }
}
