package DomainLayer;

import java.util.ArrayList;

public class Customer 
{
	private int id;
    private String name;
    private String mail;
    private String city;
    
    private ArrayList<Account> acList;
    
    public Customer(int id,String name,String mail,String city)
    {
        this.id=id;
        this.name=name;
        this.mail=mail;
        this.city=city;
    }
    
    public int getId()
    {
        return id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public String getMail()
    {
        return mail;
    }
    
    public String getCity()
    {
        return city;
    }
    
    public void setId(int id)
    {
     this.id=id;
    }
    
    public void setName(String name)
    {
     this.name=name;
    }
    
    public void setMail(String mail)
    {
     this.mail=mail;
    }
    
    public void setCity(String city)
    {
     this.city=city;
    }
    
    public void addAccount(Account a)
    {
        acList.add(a);
    }
}
