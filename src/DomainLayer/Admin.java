package DomainLayer;

import Presentation.*;
import MapperLayer.*;
import java.util.ArrayList;

public class Admin extends User 
{
	private String user;
	private String pass;
	
	private ArrayList<Employee> eList;
	private ArrayList<Transaction> eTrList;
	
	public Admin(String user, String pass)
	{
		super(user, pass);
	}
	
	public void setEmpl()
    {
        AdminMapper a=new AdminMapper();
        eList=a.getEmployeeList();
      
    }
	
	public ArrayList<Employee> getEmpl()
    {
        return eList;
    }
	
	 public void setEmplTr() 
	 {
		 AdminMapper e = new AdminMapper();
	     eTrList = e.getEmplTrList();
	 }
	 
	 public ArrayList<Transaction> getEmplTr() 
	 {    
		 return eTrList;
	 }
	 
	 public String getSelectedUser(int id)
	 {
		 for(int i=0;i<this.eList.size();i++) 
		 {
			 if(this.eList.get(i).getId() == id) 
				 return eList.get(i).getUser();
	     }
	     return "null";
	  }
	 
	 public String getSelectedPass(int id)
	 {
		 for(int i=0;i<this.eList.size();i++) 
		 {
			 if(this.eList.get(i).getId() == id) 
				 return eList.get(i).getPass();
	     }
	     return "null";
	 }
	 
	 public String getSelectedName(int id)
	 {
		 for(int i=0;i<this.eList.size();i++) 
		 {
			 if(this.eList.get(i).getId() == id) 
				 return eList.get(i).getName();
	     }
	     return "null";
	 }
	 
	 public void addEmployee(Employee e)
	 {
		 eList.add(e);
	        
		 AdminMapper a=new AdminMapper();
		 a.createEmployee(e.getId(),e.getUser(),e.getPass(),e.getName());
	 }
	 
	 public void deteleEmplById(int id)
	 {
		 //delete from arrayList	 
		 ArrayList<Employee> newList=new ArrayList<Employee>();
		 for(int i=0;i<this.eList.size();i++)
		 {
			 if(this.eList.get(i).getId() != id) 
				 newList.add(eList.get(i));
		 }
		 eList=newList;
	        
		 //delete from db
		 AdminMapper a=new AdminMapper();
		 a.deleteEmployee(id);
	 }
	 
	 public void updateEmplById(int id,String user,String pass,String name)
	 {   
		 //update in arraylist
		 ArrayList<Employee> newList=new ArrayList<Employee>();
		 for(int i=0;i<this.eList.size();i++)
		 {
			 if(this.eList.get(i).getId() != id) 
				 newList.add(eList.get(i));
			 else
			 {
				 eList.get(i).setUser(user);
				 eList.get(i).setPass(pass);
				 eList.get(i).setName(name);
			 }
		 }
		 eList=newList;
	        
		 //update in db
		 AdminMapper a=new AdminMapper();
		 a.updateEmployee(id,user,pass,name);        
	  }

}
