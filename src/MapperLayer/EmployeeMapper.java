package MapperLayer;

import DomainLayer.*;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;

public class EmployeeMapper 
{
	private Connection db = null;
    private ResultSet rs = null;
    private PreparedStatement pst = null;
    
    public boolean verifyEmployee(User l) 
    {
        String statement = "select * from employee where user=? and pass=?";
        System.out.println("user " + l.getUser() + " pass " + l.getPass());
        try 
        {
            db = Javaconnect.ConnecrDb();

            pst = db.prepareStatement(statement);
            pst.setString(1, l.getUser());
            pst.setString(2, l.getPass());
            rs = pst.executeQuery();

            if (rs.next()) 
            {
                JOptionPane.showMessageDialog(null, "Username and Password are correct");
                rs.close();
                pst.close();
                return true;
            } 
            else 
            {
                JOptionPane.showMessageDialog(null, "Data nowhere to be found...sorry");
                return false;
            }
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, e);
        }
        return false;
    }
    
    public ArrayList<Customer> getCustomerList() 
    {
        ArrayList<Customer> cList = new ArrayList<Customer>();
        
        String sql = "select * from customer";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) 
            {
                Customer c = new Customer(0, "0", "0", "0");
                c.setId(rs.getInt("idCustomer"));
                c.setName(rs.getString("name"));
                c.setMail(rs.getString("mail"));
                c.setCity(rs.getString("city"));

                cList.add(c);
            }

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "getcList " + e);
        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            }
            catch (Exception e) 
            {
            }
        }
        
        return cList;
    }
    
    public void updateCustomer(int id, String name, String mail, String city) 
    {
        String sql = "update customer set name=? , mail=? , city=? where idCustomer=?";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);

            pst.setString(1, name);
            pst.setString(2, mail);
            pst.setString(3, city);
            pst.setInt(4, id);
            int rs = pst.executeUpdate();

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "upc " + e);
            System.out.println("upc " + e);

        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public void createCustomer(int id, String name, String mail, String city) 
    {
        String sql = "insert into customer values(?,?,?,?)";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            pst.setInt(1, id);
            pst.setString(2, name);
            pst.setString(3, mail);
            pst.setString(4, city);
            int rs = pst.executeUpdate();

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "createc " + e);
            System.out.println("createc " + e);

        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public ArrayList<Account> getAccountList() 
    {
        ArrayList<Account> cList = new ArrayList<Account>();
        
        String sql = "select * from account";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) 
            {
                Account c = new Account(0, 0);
                c.setId(rs.getInt("idAccount"));
                c.setType(rs.getString("actID"));
                c.setCustid(rs.getString("idCustomer"));
                c.setBalance(rs.getString("balance"));
                c.setCurrency(rs.getString("currency"));
                c.setCrDate(rs.getString("cDate"));

                cList.add(c);
            }

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "get customer List " + e);
        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
        
        return cList;
    }
    
    public void updateAccount(int id, String type, String cid, String balance, String currency, String date) 
    {
        String sql = "update account set  actID=? , idCustomer=?, balance=?, currency=?,cDate=? where idAccount=? ";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);

            pst.setString(1, type);
            pst.setString(2, cid);
            pst.setString(3, balance);
            pst.setString(4, currency);
            pst.setString(5, date);
            pst.setInt(6, id);
            int rs = pst.executeUpdate();

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "update customer " + e);
            System.out.println("update customer " + e);

        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public void deleteAccount(int aid) 
    {

        String sql = "delete from account where idAccount=?";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            pst.setInt(1, aid);
            int rs = pst.executeUpdate();

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "deete account " + e);
            System.out.println("delete account " + e);

        } 
        finally 
        {
            try
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public void withdrawAccount(int aid, int newBalance) 
    {

        String sql = "update account set balance=? where idAccount=? ";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);

            pst.setInt(1, newBalance);
            pst.setInt(2, aid);
            int rs = pst.executeUpdate();

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "update balance " + e);
            System.out.println("update balance " + e);

        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public void depositAccount(int aid, int newBalance) 
    {

        String sql = "update account set balance=? where idAccount=? ";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);

            pst.setInt(1, newBalance);
            pst.setInt(2, aid);
            int rs = pst.executeUpdate();

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "update employee " + e);
            System.out.println("update employee " + e);

        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public void createAccount(int id, int type, int custId, int balance, String currency, String crDate) 
    {
        String sql = "insert into account values(?,?,?,?,?,?)";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            pst.setInt(1, id);
            pst.setInt(2, type);
            pst.setInt(3, custId);
            pst.setInt(4, balance);
            pst.setString(5, currency);
            pst.setString(6, crDate);
            int rs = pst.executeUpdate();

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "create account " + e);
            System.out.println("create account " + e);
        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public ArrayList<Transaction> getAccountTrList() 
    {

        ArrayList<Transaction> accTrList = new ArrayList<Transaction>();
        
        String sql = "select * from transaction";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) 
            {
                Transaction c = new Transaction();
                c.setId(rs.getInt("idTransaction"));
                c.setEmpId(rs.getInt("eId"));
                c.setCustId(rs.getInt("cId"));
                int tip = rs.getInt("trtid");
                switch (tip) 
                {
                    case 1:
                        c.setType("transfer");
                        break;
                    case 2:
                        c.setType("deposit");
                        break;
                    case 3:
                        c.setType("withdraw");
                        break;
                }
              

                c.setAmount(rs.getInt("amount"));
                c.setDate(rs.getString("date"));

                accTrList.add(c);
            }
        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "getcList " + e);

        } 
        finally
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
        
        return accTrList;
    }
    
    public void createTr(int id, int empId, int custId, int acc, String type, int amount, String crDate2) 
    {

        String sql = "insert into transaction values(?,?,?,?,?,?)";
        try 
        {
            db = Javaconnect.ConnecrDb();
            pst = db.prepareStatement(sql);
            pst.setInt(1, id);
            pst.setInt(2, empId);
            pst.setInt(3, custId);
            //pst.setInt(4, acc);
            switch (type) 
            {
                case "withdraw":
                    pst.setInt(4, 3);
                    break;
                case "deposit":
                    pst.setInt(4, 2);
                    break;
                case "transfer":
                    pst.setInt(4, 1);
                    break;
            }
            // pst.setString(4, type);
            pst.setInt(5, amount);
            pst.setString(6, crDate2);
            int rs = pst.executeUpdate();

        } 
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "createtr " + e);
            System.out.println("createtr " + e);

        } 
        finally 
        {
            try 
            {
                rs.close();
                pst.close();
            } 
            catch (Exception e) 
            {
            }
        }
    }
    
    public int getEmployeeId(String user) 
    {    
        String statement = "select idEmployee from employee where user=?";
       int id=1;
       try 
       {
           db = Javaconnect.ConnecrDb();
           //Session session = getSessionFactory().getCurrentSession();
           pst = db.prepareStatement(statement);
           pst.setString(1,user );
           rs = pst.executeQuery();
           System.out.println(id+" id sql1");
           
          // id=rs.getInt("idEmployee");
           if (rs.next()) 
           {
               id=rs.getInt("idEmployee");
               System.out.println(id+" id sql2");
               rs.close();
               pst.close();
               return id;
           } 
           else 
           {
              //JOptionPane.showMessageDialog(null, "Data nowhere to be found...sorry");
               return id;
           }
       } 
       catch (Exception e) 
       {
           JOptionPane.showMessageDialog(null, e);
       }
       return id;
   }
}
